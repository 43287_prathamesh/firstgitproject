#include<stdio.h>
#include<stdlib.h>

/*
int main(void)
{
    printf("Good\rGreat Morning \n"); 
        //  Great Morning
        //\r carriage return
        // it takes the cursor on first character of same line
        //and start overwriting

        //\r brings the curson on next line and start writing
        //Good
        //Great Morning
    return 0;
}
*/

/*
int main(void)
{
    printf("DAY0\b2\n");
          //DAY2
    // \b indicates back space 
    return 0;
}
*/

/*
int main(void)
{
    printf("ASCII VALUE OF CHARCTER A is %d \n",'A');
    printf("\* SEPT 2020\* \n");
    return 0;
}
*/


/*

// ESCAPE SEQUENCES
//\n \r \t \b \' \" \$ \*

// "C FOUNDATION"
// DAY02    Fundamentals
//"DAY02\Program5th"

int main(void)
{
    printf("\"C FOUNDATION\"");
    printf("\nDAY02\t Fundamentals");
    printf("\n\"DAY02\\Program5th\" \n");
    return 0;

}
*/



/*

//Number System 
//OCTAL : %o
//HEXA : %x 

int main(void)
{
    int a=40;
    printf("A = %d ",a);
    printf("A in octal = %o ",a);
    printf("A in hexa = %x \n",a);
    return 0;
}
*/


/*
int main(void)
{
    int i;
    float f;
    char ch;
    double d;

    printf("size = %d \n",sizeof(i+ch+d));

    return 0;
}
*/

/*
int main(void)
{
    int i=20;
    char ch='S';
    float f=5.5;
    double d=6.7;

   // printf("Size = %d ",sizeof(int));
    //printf("\n size of variable i = %d ",sizeof(i));
    //printf("\n size of any number = %d \n",sizeof(50));

    
    //printf("Size = %d",sizeof(char));
    //printf("\n Sizeof variable = %d ",sizeof(ch));
    //printf("\n size of any character = %d \n",sizeof('A'));
                                        //sizeof('A')
                                        //sizeof(65)
    //  ASCII CHARACTER SET A : 65 B : 66...
    //a : 97 b : 98 ....


    //printf("size=%d ",sizeof(float));
    //printf("\n size of variable = %d ",sizeof(f));
   // printf("\n size of direct value = %d \n",sizeof(8.3));
    //by defualt compiler converts any decimal number 
    //as double if it is passed to sizeof

    //printf("\n size of direct value = %d \n",sizeof(8.3f));


    printf("\n size = %d ",sizeof(double));
    printf("\ size of d variable = %d ",sizeof(d));
    printf("\n size of any decimal number = %d \n",sizeof(4.2));


    return 0;
}
*/

/*
int main(void)
{
    int i=10; // i is variable name
        //data type of i variabale is int

        //variable initialization
    float f=5.4;
    char ch='A';
    double d=6.3;

    printf(" I VALUE = %d F VALUE = %f CHAR CH=%c \n",i,f,ch);
    
    return 0;
}
*/
