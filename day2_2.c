#include<stdio.h>
#include<stdlib.h>




//formatting the output

int main(void)
{
    int num1=30;
    int num2=5000;
    float f=5.46327;
    printf("%d %d %f",num1,num2,f);
    printf("\n----------------");

    printf("\n%7d %7d %.2f",num1,num2,f);
    // _ _ _ _ _ 3 0 _ _ _ 5 0 0 0 5.46
    printf("\n%7d %7d %.2f",num2,num1,f);
    printf("\n----------------");
    printf("\n%-7d %-7d %8.2f \n",num1,num2,f);
    // 3 0 _ _ _ _ _   5 0 0 0 _ _ _    _ _ _ _ 5 . 4 6

    printf("\n%-7d %-7d %8.2f \n",num2,num1,f);
    printf("\n-----------------");
    printf("%09d",num1);
    

    return 0;

}



/*
int main(void)
{
    int num;
    char ch;

    printf("Enter Numeric Value : ");
    scanf("%d",&num);
    printf("Enter One character : ");
    scanf("%*c"); //suppress the meaning of new line character
    scanf("%c",&ch);
    printf("\ Number = %d Ch = %c \n",num,ch);
    return 0;

    //\n (new line character) is considered as a single character


}
*/



/*
int main(void)
{
    int num1,num2;
    printf("Enter first number = ");
    scanf("%d",&num1);
    printf("\n Enter Second number = ");
    scanf("%d",&num2);
    printf("Num1 = %d Num2 =%d \n",num1,num2);
    return 0;
}
*/