#include<stdio.h>
#include<stdlib.h>



int main(void)
{
    int a=5;
    printf("%d %d %d\n",a,a++,++a);
    //a    a++     ++a
    //7     6        6
    //if we recieve multiple number of 
    //pre and post opearations in printf statemtne
    //then we move from right to left
    return 0;
}


/*
int main(void)
{
    int num1=20,num2=25;
    int max = (num1>num2)?num1:num2;

    //Conditional / ternary operator(?:)
    // Expression ? True : false 


    printf("Max = %d \n",max);
}
*/



/*
int main(void)
{
    int a=0;
    int b=4,c=5;
    int ans;
    printf("A = %d B = %d C=%d",a,b,c);
    ans=a && b++||c;
     // E1 && E2 || E3
     // 0  && __ || E3
     //   0      || E3
     //   0      || 1
     //      1 (TRUE)

    //ans = 1
        
    //  E1 : a   E1 : 0 false  E1 : 0
    //E2 : b++ NOT EVALUATED
    //E3 : c : 5 E3 : true (1 ) 

    printf("\n A = %d B = %d C=%d Ans=%d \n",a,b,c,ans);
    printf("\n Ans = %d \n",!ans);
    return 0;

}

*/
//!5
//!Expression1
//!non zero number
//!1
//0


/*
int main(void)
{
    int a=-9,b=3,c=0,d;
    printf("A=%d B = %d C=%d ",a,b,c);
    d= a++ || ++b && c++;
     //E1  ||  E2 && E3
     // 1  ||  E2 && E3 
     //d=1

    //a++ ===> Expression 1 
    //++b ===> Expresion2 
    //c++ ===> Expression3

    //E1 : a++
    //     -9++ 
    //E1 : -9  a= -8
    // Expression 1 is holding non zero value 
    //Expression1 is returning True (1)


    printf("\n A=%d B = %d C=%d D=%d \n",a,b,c,d);
    return 0;
}
*/

/*

int main(void)
{
    int num1,num2;
    printf("Enter Num1 : ");
    scanf("%d",&num1);

    printf("Enter Num2 : ");
    scanf("%d",&num2);

    printf("\n Num1==Num2 = %d",num1==num2);
    printf("\n Num1!=Num2 = %d ",num1!=num2);
    printf("\n Num1<Num2 = %d",num1<num2);
    printf("\n Num1<=Num2 = %d",num1<=num2);
    printf("\n Num1>Num2 = %d ",num1>num2);
    printf("\n Num1>=Num2 = %d \n",num1>=num2);


    //true : 1
    //false  : 0
    return 0;
}
*/

/*
int main(void)
{
    int num=10;
    int a;
    a=++num; //pre incremenet operation
    //It will first performs the operation and then 
    //It assigns the value 
    
    printf("Num = %d A = %d \n",num,a);

    return 0;
}
*/


/*
int main(void)
{
    int num=10;
    int a;
    a=num++; //post incremenet operation
    //It assigns the value first 
    //and then it performs incr/decr operation
    printf("Num = %d A = %d \n",num,a);

    return 0;
}
*/

/*
int main(void)
{
    int num=40;
    num+=5; //num= num+5;
    printf("Num : %d ",num);
    num-=20;//num=num-20;
    printf("\n Num : %d\n ",num);
    return 0;
}

*/


/*
int main(void)
{
    int num1=20,num2=10;


    //Arithmetic operators
    printf("%d %d %d %d \n",num1+num2, num1-num2,num1*num2,num1/num2);
    printf("\n Mod : %d \n",num1%num2); //REmainder

    return 0;
}

// Split the number into digits 
//541 541/10 ===> 54   541%10===> 1 Digit 1 
//  54 / 10 ==> 5   54%10 ==> 4 Digit 2
//5/10 ===> 0 5%10 ==> 5 Digit3

//face value : 5*100 = 4*10 = 40 1*1=1 
//place value : digit 3 is at hundred's place

//5
//4
//1 

*/