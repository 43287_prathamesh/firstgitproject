//File inclusion area 
//Comments  (Documentation purpose)(non executable statement)
//Program 1 of C Foundation
/*
 Author : Akshita
Sunbeam
DAY01 -C
*/ 
#include<stdio.h> 
#include<stdlib.h>

//entry point function


int main(void) 
{
    //definition part 
    printf("Hello!!Welcome to sunbeam C Foundation Course\n");
    //stdout (standard output screen)

    return 0;//return EXIT_SUCCESS
}//SCOPE / LIFE


//main function is returning integer value 
//returrn type (int)

//Compile the program (convert high level language into machine level [COMPILER]) GCC
//gcc prgmname.c will generate
//exe(windows) or out (ubuntu) file 
//Execute the program
//./a.exe OR ./a.out

